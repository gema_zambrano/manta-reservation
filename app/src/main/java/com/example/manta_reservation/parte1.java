package com.example.manta_reservation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class parte1 extends AppCompatActivity {
    Button button11,button12,button8,button7,button6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parte1);

        button11=findViewById(R.id.button11);
        button12=findViewById(R.id.button12);
        button8=findViewById(R.id.button8);
        button7=findViewById(R.id.button7);
        button6=findViewById(R.id.button6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte1.this,favoritos.class);
                startActivity(intent2);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte1.this,ofertas.class);
                startActivity(intent2);
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte1.this,configuracion.class);
                startActivity(intent2);
            }
        });
        button12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte1.this,parte2.class);
                startActivity(intent2);
            }
        });

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte1.this,parte2.class);
                startActivity(intent2);
            }

        });
    }
}
